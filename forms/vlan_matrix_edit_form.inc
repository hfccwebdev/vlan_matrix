<?php

/**
 * @file
 * Provides the VLAN Matrix editing form.
 *
 * @see vlan_matrix.module
 */

/**
 * VLAN Matrix form.
 *
 * @see vlan_matrix_edit_submit()
 * @ingroup forms
 */
function vlan_matrix_edit_form($form, &$form_state, $entity) {

  $form['entity'] = [
    '#type' => 'value',
    '#value' => $entity,
  ];

  $form['vlan_id'] = [
    '#type' => 'textfield',
    '#title' => t('VLAN ID'),
    '#default_value' => !empty($entity->vlan_id) ? $entity->vlan_id : NULL,
    '#size' => 5,
    '#maxlength' => 5,
    '#required' => TRUE,
    '#disabled' => !empty($entity->vlan_id),
  ];

  $form['vlan_name'] = [
    '#type' => 'textfield',
    '#title' => t('VLAN Name'),
    '#default_value' => !empty($entity->vlan_name) ? trim($entity->vlan_name) : NULL,
    '#description' => t('Enter the name of this VLAN as defined by VTP on the network core switch.'),
    '#size' => 15,
    '#maxlength' => 15,
    '#required' => FALSE,
  ];

  $form['vlan_subnet'] = [
    '#type' => 'textfield',
    '#title' => t('Subnet'),
    '#default_value' => !empty($entity->vlan_subnet) ? trim($entity->vlan_subnet) : NULL,
    '#description' => t('Enter the VLAN subnet in CIDR <em>(n.n.n.n/n)</em> format.'),
    '#size' => 25,
    '#maxlength' => 25,
    '#required' => FALSE,
  ];

  $form['vlan_gateway'] = [
    '#type' => 'textfield',
    '#title' => t('Gateway IP'),
    '#description' => t('If this subnet has an IP address on the 6509, enter the node address here.'),
    '#default_value' => !empty($entity->vlan_gateway) ? trim($entity->vlan_gateway) : NULL,
    '#size' => 12,
    '#maxlength' => 12,
  ];

  $form['vlan_status'] = [
    '#type' => 'select',
    '#title' => t('Status'),
    '#default_value' => !empty($entity->vlan_status) ? trim($entity->vlan_status) : NULL,
    '#options' => $entity->statusOptions(),
  ];

  $form['vlan_comments'] = [
    '#type' => 'textfield',
    '#title' => t('Comments'),
    '#default_value' => !empty($entity->vlan_comments) ? trim($entity->vlan_comments) : NULL,
    '#size' => 50,
    '#maxlength' => 255,
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => 'Save',
  ];

  return $form;
}

/**
 * Validate VLAN Matrix Edit form submissions.
 */
function vlan_matrix_edit_form_validate($form, &$form_state) {
  $entity = $form_state['values']['entity'];
  if (isset($entity->is_new) && $entity->is_new) {
    $vlan_id = $form_state['values']['vlan_id'];
    $vlan_check = db_query("SELECT vlan_id FROM {vlan_matrix} WHERE vlan_id = :id", [':id' => $vlan_id])->fetchAll();
    if(!empty($vlan_check)) {
      form_set_error('vlan_id', t("VLAN %vlan_id is already in use. Please select another value.", ['%vlan_id' => $vlan_id]));
    }
  }
}

/**
 * Process VLAN Matrix Edit form submissions.
 *
 * @see vlan_matrix_edit_form()
 */
function vlan_matrix_edit_form_submit($form, &$form_state) {
  $entity = $form_state['values']['entity'];

  if (isset($entity->is_new) && $entity->is_new) {
    $entity->vlan_id = $form_state['values']['vlan_id'];
  }
  $entity->vlan_name = $form_state['values']['vlan_name'];
  $entity->vlan_subnet = $form_state['values']['vlan_subnet'];
  $entity->vlan_gateway = $form_state['values']['vlan_gateway'];
  $entity->vlan_status = $form_state['values']['vlan_status'];
  $entity->vlan_comments = $form_state['values']['vlan_comments'];
  entity_save('vlan_matrix', $entity);
  drupal_goto('vlan-matrix');
}
