<?php

/**
 * @file
 * Provides the VLAN Matrix delete confirmation form.
 *
 * @see vlan_matrix.module
 */

/**
 * Entity delete confirmation form.
 */
function vlan_matrix_delete_confirm_form($form, &$form_state, $entity) {

  $form['entity'] = ['#type' => 'value', '#value' => $entity];
  $form['id'] = ['#type' => 'value', '#value' => $entity->identifier()];

  $question = t('Are you sure you want to <em>delete</em> %label?', ['%label' => $entity->label()]);
  $path = 'vlan-matrix';
  $description = t('This action cannot be undone.');
  return confirm_form($form, $question, $path, $description, t('Delete'), t('Cancel'));
}

/**
 * Submit handler for deletion form.
 */
function vlan_matrix_delete_confirm_form_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $entity = $form_state['values']['entity'];
    entity_delete('vlan_matrix', $form_state['values']['id']);
    $form_state['redirect'] = 'vlan-matrix';
  }
}
