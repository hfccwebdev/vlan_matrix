<?php

/**
 * The VlanMatrix interface.
 *
 * Note: Interfaces must be in the same file as their classes
 * for Drupal 7's autoloader to find them.
 *
 */
interface VlanMatrixInterface {

  /**
   * Gets the identifier using Drupal 8 syntax.
   *
   * @return int
   *   The Entity ID.
   */
  public function id();

  /**
   * Generates the HTML for a link to this entity.
   *
   * @param string|null $text
   *   (optional) The link text for the anchor tag as a translated string.
   *   If NULL, it will use the entity's label. Defaults to NULL.
   *
   * @return string
   *   A Link to the entity.
   */
  public function toLink($text = NULL);

  /**
   * Maintainer actions.
   *
   * @return array
   *   An array of links to allowed actions, if any.
   */
  function maintainerActions();

  /**
   * Generates an edit path for this entity.
   *
   * @return string
   *   The edit path.
   */
  public function editPath();

  /**
   * Generates a delete path for this entity.
   *
   * @return string
   *   The delete path.
   */
  public function deletePath();

  /**
   * Get the formatted status.
   *
   * @return string
   *   The formatted name of the current vlan_status.
   */
  public function getStatus();

  /**
   * Possible VLAN status options.
   *
   * @return array
   *   An associative array of options.
   */
  public static function statusOptions();

}

/**
 * The VlanMatrix entity class.
 */
class VlanMatrix extends Entity implements VlanMatrixInterface {

  /**
   * {@inheritdoc}
   */
  protected function defaultLabel() {
    return t('VLAN @id: @title', ['@id' => $this->identifier(), '@title' => $this->vlan_name]);
  }

  /**
   * {@inheritdoc}
   */
  protected function defaultUri() {
    return ['path' => 'vlan-matrix/' . $this->identifier()];
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->identifier();
  }

  /**
   * {@inheritdoc}
   */
  public function toLink($text = NULL) {
    if (!isset($text)) {
      $text = $this->label();
    }
    else {
      $text = check_plain($text);
    }
    return l($text, $this->uri()['path']);
  }

  /**
   * {@inheritdoc}
   */
  function maintainerActions() {
    $actions = [];
    if (entity_access('update', 'vlan_matrix' , $this->id())) {
      $actions[] = l(t('edit'), $this->editPath());
    }
    return !empty($actions) ? $actions : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function editPath() {
    return $this->uri()['path'] . '/edit';
  }

  /**
   * {@inheritdoc}
   */
  public function deletePath() {
    return $this->uri()['path'] . '/delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    if (!empty($this->vlan_status) && !empty($this->statusOptions()[$this->vlan_status])) {
      return $this->statusOptions()[$this->vlan_status];
    }
    else {
      return check_plain($this->vlan_status);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function statusOptions() {
    return [
      'LIVE' => 'Live',
      'LEGACY' => 'Legacy',
      'LOCAL' => 'Local',
      'NO' => 'No',
      'PROPOSED' => 'Proposed',
      'SPECIAL' => 'Special',
    ];
  }
}
