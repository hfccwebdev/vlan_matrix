<?php

/**
 * Interface for Pages service class.
 */
interface VlanMatrixPagesInterface {

  /**
   * Displays the full VLAN Matrix table.
   *
   * @return array
   *   A renderable array.
   */
  public static function displayAll();

  /**
   * Displays a single VLAN entity.
   *
   * @param VlanMatrix $vlan
   *   The VLAN entity to display.
   *
   * @return array
   *   A renderable array.
   */
  public static function displayVlan($vlan);

  /**
   * Creates a new entity ready for editing.
   *
   * @param array $values
   *   An array of values to populate entity properties.
   *
   * @return
   *   Returns the editing form populated with the new entity values.
   */
  public static function addNew($values);

}

/**
 * Provides page callback services for the vlan_matrix module.
 *
 * @see vlan_matrix.module
 */
class VlanMatrixPages implements VlanMatrixPagesInterface {

  /**
   * {@inheritdoc}
   */
  public static function displayAll() {
    $header = [t('VLAN'), t('Name'), t('Subnet'), t('Gateway'), t('Status'), t('Comments'), t('Actions')];
    $rows = [];
    $ids = db_query("SELECT vlan_id FROM {vlan_matrix} ORDER BY vlan_id")->fetchCol();
    $result = entity_load('vlan_matrix', $ids);
    foreach ($result as $vlan) {
      $rows[] = [
        $vlan->id(),
        check_plain($vlan->vlan_name),
        check_plain($vlan->vlan_subnet),
        check_plain($vlan->vlan_gateway),
        $vlan->getStatus(),
        check_plain($vlan->vlan_comments),
        !empty($vlan->maintainerActions()) ? implode(' ', $vlan->maintainerActions()) : NULL,
      ];
    }
    return [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => ['class' => ['vlan-matrix']],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function displayVlan($vlan) {
    return entity_view('vlan_matrix', [$vlan], 'full', NULL, TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public static function addNew($values = []) {
    $entity = entity_create('vlan_matrix', $values);
    // So annoying...
    module_load_include('inc', 'vlan_matrix', 'forms/vlan_matrix_edit_form');
    return drupal_get_form('vlan_matrix_edit_form', $entity);
  }
}
