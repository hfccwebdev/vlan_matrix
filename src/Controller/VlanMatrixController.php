<?php

/**
 * The EntityAPIController extension for VlanMatrix entities.
 */
class VlanMatrixController extends EntityAPIController {

  /**
   * {@inheritdoc}
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = []) {
    if (!empty($entity->vlan_name)) {
      $content['vlan_name'] = [
        '#field_name' => 'vlan_name',
        '#weight' => -9,
        '#label' => t('VLAN Name'),
        '#label_display' => 'inline',
        '#theme' => 'hfcc_global_pseudo_field',
        '#markup' => check_plain($entity->vlan_name),
      ];
    }
    if (!empty($entity->vlan_subnet)) {
      $content['vlan_subnet'] = [
        '#field_name' => 'vlan_subnet',
        '#weight' => -8,
        '#label' => t('Subnet'),
        '#label_display' => 'inline',
        '#theme' => 'hfcc_global_pseudo_field',
        '#markup' => check_plain($entity->vlan_subnet),
      ];
    }
    if (!empty($entity->vlan_gateway)) {
      $content['vlan_gateway'] = [
        '#field_name' => 'vlan_gateway',
        '#weight' => -7,
        '#label' => t('Gateway'),
        '#label_display' => 'inline',
        '#theme' => 'hfcc_global_pseudo_field',
        '#markup' => check_plain($entity->vlan_gateway),
      ];
    }
    if (!empty($entity->vlan_status)) {
      $content['vlan_status'] = [
        '#field_name' => 'vlan_status',
        '#weight' => -6,
        '#label' => t('Status'),
        '#label_display' => 'inline',
        '#theme' => 'hfcc_global_pseudo_field',
        '#markup' => $entity->getStatus(),
      ];
    }
    if (!empty($entity->vlan_comments)) {
      $content['vlan_comments'] = [
        '#field_name' => 'vlan_comments',
        '#weight' => -5,
        '#label' => t('Comments'),
        '#label_display' => 'inline',
        '#theme' => 'hfcc_global_pseudo_field',
        '#markup' => check_plain($entity->vlan_comments),
      ];
    }
    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }

}
